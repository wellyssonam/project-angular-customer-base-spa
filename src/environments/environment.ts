// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBy88N1-TnQR2rF-p3uUgsDVBOE0QoIt3s",
    authDomain: "banco-dfb6a.firebaseapp.com",
    databaseURL: "https://banco-dfb6a.firebaseio.com",
    projectId: "banco-dfb6a",
    storageBucket: "banco-dfb6a.appspot.com",
    messagingSenderId: "582694126557",
    appId: "1:582694126557:web:da36b6ede8d0bd264b2c34"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
