import { ClientViewModel } from './../../client/models/client-view-model';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ClientFormComponent } from '../client-form/client-form.component';
import { ClientService } from 'src/app/client/services/client.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private serviceClient: ClientService
  ) { }

  clients: ClientViewModel[] = []

  ngOnInit() {
    this.showClients()
  }

  handleModalClientForm = (response) => {
    // ação após fechar modal
    console.log('teste')
    console.log(response)
    if (response === Object(response)) {
      console.log(response)
      if(response.modeInsert) {
        console.log('insert')
        response.client.id = response.id
        // unshift = adiciona na list
        this.clients.unshift(response.client)
      } else {
        console.log('edit')
        let index = this.clients.findIndex(value => value.id === response.id)
        this.clients[index] = response.client
      }
    }
  }
  
  addClient = () => {
    const modal = this.modalService.open(ClientFormComponent);
    modal.result.then(
      this.handleModalClientForm.bind(this),
      this.handleModalClientForm.bind(this)
    )
  }

  showClients = () => {
    this.serviceClient.getClients().subscribe(response => {
      console.log(response.docs)
      this.clients = []
      response.docs.forEach(value => {
        const data = value.data()
        const id = value.id
        console.log(data)
        console.log(value.id)
        console.log(typeof(value.id))
        const client: ClientViewModel = {
          id: id,
          name: data.name,
          address: data.address,
          married: data.married,
          dateUpd: data.dateUpd.toDate()
        }
        this.clients.push(client)
      });
    });
  }

  checkedMarried = (index: number) => {
    const newValue = !this.clients[index].married
    this.clients[index].married = newValue
    const obj = {married: newValue}
    const id = this.clients[index].id
    this.serviceClient.editPartialClient(id, obj)
  }

  EditClick = (client: ClientViewModel) => {
    const modal = this.modalService.open(ClientFormComponent);
    modal.result.then(
      this.handleModalClientForm.bind(this),
      this.handleModalClientForm.bind(this)
    )
    modal.componentInstance.modeInsert = false
    modal.componentInstance.client = client
  }

  DeleteClick = (clientId: string, index: number) => {
    this.serviceClient.deleteClient(clientId)
      .then(() => { this.clients.splice(index) })
      .catch(err => console.error(err))
  }
}
