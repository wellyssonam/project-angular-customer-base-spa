export interface Client {
    name: string,
    address: string,
    married: boolean,
    dateIni: Date,
    dateUpd: Date
}