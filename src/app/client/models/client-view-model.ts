export interface ClientViewModel {
    id: string,
    name: string,
    address: string,
    married: boolean,
    dateUpd: Date
}
