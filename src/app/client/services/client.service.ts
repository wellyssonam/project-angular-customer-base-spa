import { ClientViewModel } from './../models/client-view-model';
import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Client } from '../models/client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private db: AngularFirestore) { } 

  private clientColection = 'clients'

  getClients(): Observable<firebase.firestore.QuerySnapshot>{
    return this.db.collection<Client>(this.clientColection, ref => ref.orderBy('name', 'asc')).get()
  }

  saveClient(client: Client): Promise<DocumentReference>{ 
    return this.db.collection(this.clientColection).add(client)
  }

  editClient(client: ClientViewModel): Promise<void>{ 
    return this.db.collection(this.clientColection).doc(client.id).update(client)
  }

  editPartialClient(id: string, obj: Object): Promise<void>{ 
    return this.db.collection(this.clientColection).doc(id).update(obj)
  }

  deleteClient(id: string): Promise<void>{ 
    return this.db.collection(this.clientColection).doc(id).delete()
  }
}
