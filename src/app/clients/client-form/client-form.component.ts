import { DocumentReference } from '@angular/fire/firestore';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Client } from 'src/app/client/models/client';
import { ClientService } from 'src/app/client/services/client.service';
import { ClientViewModel } from 'src/app/client/models/client-view-model';

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.css']
})
export class ClientFormComponent implements OnInit {

  clientForm: FormGroup
  client: ClientViewModel
  modeInsert: boolean = true

  constructor(
    private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    private serviceClient: ClientService,
  ) { }

  ngOnInit() {
    this.clientForm = this.formBuilder.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      married: false
    })

    if(!this.modeInsert) {
      this.loadClient(this.client)
    }
  }

  loadClient = (client) => {
    this.clientForm.patchValue(client)
  }

  saveClient = () => {
    if(this.clientForm.invalid) {
      return
    }

    if(this.modeInsert) {
      let client: Client = this.clientForm.value;
      client.dateIni = new Date()
      client.dateUpd = new Date()
      this.serviceClient.saveClient(client)
        .then(response => this.handleSuccessSave(response, client))
        .catch(err => console.error(err))
    } else {
      let client: ClientViewModel = this.clientForm.value
      client.id = this.client.id
      client.dateUpd = new Date()
      this.serviceClient.editClient(client)
        .then(() => this.handleSuccessEdit(client))
        .catch(err => console.error(err))
    }
  }

  handleSuccessSave = (response: DocumentReference, client: Client) => {
    this.activeModal.dismiss({client: client, id: response.id, modeInsert: true})
  }
  
  handleSuccessEdit = (client: ClientViewModel) => {
    this.activeModal.dismiss({client: client, id: client.id, modeInsert: false})
  }
}
